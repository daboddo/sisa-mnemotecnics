(defsystem "SISA"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ("str")
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "SISA/tests"))))

(defsystem "SISA/tests"
  :author ""
  :license ""
  :depends-on ("SISA"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for SISA"
  :perform (test-op (op c) (symbol-call :rove :run c)))
